import pickle as pkl
import numpy as np
import matplotlib.pyplot as plt
import knn
import learn
import predict

'''
def hog_all(x):
    images = list(map(lambda xi: xi.reshape(36, 36), x))
    return list(map(lambda image: hog(image).ravel(), images))


def hog(image):
    nwin_x = 5
    nwin_y = 5
    B = 7
    (L, C) = np.shape(image)
    H = np.zeros(shape=(nwin_x * nwin_y * B, 1))
    if C is 1:
        raise NotImplementedError
    step_x = np.floor(C / (nwin_x + 1))
    step_y = np.floor(L / (nwin_y + 1))
    cont = 0
    hxy = [1, 0, -1]
    grad_xr = np.convolve(image.ravel(), hxy, mode='same').reshape(36, 36)
    grad_yu = np.convolve(image.T.ravel(), hxy, mode='same').reshape(36, 36).T
    angles = np.arctan2(grad_yu, grad_xr)
    magnit = np.sqrt((grad_yu ** 2 + grad_xr ** 2))
    for n in range(nwin_y):
        for m in range(nwin_x):
            cont += 1
            angles2 = angles[int(n * step_y):int((n + 2) * step_y), int(m * step_x):int((m + 2) * step_x)]
            magnit2 = magnit[int(n * step_y):int((n + 2) * step_y), int(m * step_x):int((m + 2) * step_x)]
            v_angles = angles2.ravel()
            v_magnit = magnit2.ravel()
            K = np.shape(v_angles)[0]
            bin = 0
            H2 = np.zeros(shape=(B, 1))
            for ang_lim in np.arange(start=-np.pi + 2 * np.pi / B, stop=np.pi + 2 * np.pi / B, step=2 * np.pi / B):
                for k in range(K):
                    if v_angles[k] < ang_lim:
                        v_angles[k] = 100
                        H2[bin] += v_magnit[k]
                bin += 1

            H2 = H2 / (np.linalg.norm(H2) + 0.01)
            H[(cont - 1) * B:cont * B] = H2
    return H


with open('train.pkl', 'rb') as f:
    data = pkl.load(f)


x = data[0]
y = data[1]

output = open('newFile.pkl', 'wb')
pkl.dump((hog_all(x), y), output)





# x = data[:, 0]
learn.learn()
# print(np.reshape(x, (36, 36)))
a = data.shape[0]
b = np.zeros(shape=(a, 1)).astype(int)
print(b)
model = LogisticRegression

with open('train.pkl', 'rb') as f:
    data = pkl.load(f)[0]
print(data)



def softmax(W):
    expW = np.exp(W)
    return expW / expW.sum()


def sigmoid(x):
    return 1 / (1 + np.exp(-x))


def gradientSigmoid(x):
    return sigmoid(x) * (1 - sigmoid(x))

with open('train.pkl', 'rb') as f:
    feature_set = (pkl.load(f)[0])[0:500, 0:1296]

with open('train.pkl', 'rb') as f:
    laabels = (pkl.load(f)[1])[0:500]

#feature_set = tSet[0:50000, 0:1296]
#laabels = tLabel[0:50000]

one_hot_labels = np.zeros((50000, 10))
for i in range(50000):
    one_hot_labels[i, laabels[i]] = 1

wh = np.random.rand(1296, 4)
bh = np.random.randn(4)

wo = np.random.rand(4, 10)
bo = np.random.randn(10)
lr = 10e-4

error_cost = []



for epoch in range(50000):
        ############# feedforward

        # Phase 1
        zh = np.dot(one_hot_labels, wh) + bh
        ah = sigmoid(zh)

        # Phase 2
        zo = np.dot(ah, wo) + bo
        ao = softmax(zo)

        ########## Back Propagation

        ########## Phase 1

        dcost_dzo = ao - one_hot_labels
        dzo_dwo = ah

        dcost_wo = np.dot(dzo_dwo.T, dcost_dzo)

        dcost_bo = dcost_dzo

        ########## Phases 2

        dzo_dah = wo
        dcost_dah = np.dot(dcost_dzo, dzo_dah.T)
        dah_dzh = gradientSigmoid(zh)
        dzh_dwh = feature_set
        dcost_wh = np.dot(dzh_dwh.T, dah_dzh * dcost_dah)

        dcost_bh = dcost_dah * dah_dzh

        # Update Weights ================

        wh -= lr * dcost_wh
        bh -= lr * dcost_bh.sum(axis=0)

        wo -= lr * dcost_wo
        bo -= lr * dcost_bo.sum(axis=0)

        if epoch % 200 == 0:
            loss = np.sum(-one_hot_labels * np.log(ao))
            print('Loss function value: ', loss)
            error_cost.append(loss)
learn.learn()            

7 - 0.3268
9 - 0.3318
7 - 0.3288
7 - 0.336
5 - 0.3254
5 - 0.3242
8 - 0.3168
8 - 0.3168
7 - 0.326
6 - 0.3258
8 - 0.3252
10- 0.3324
10- 0.3324
'''
s = 30000
t = 10000

with open('train.pkl', 'rb') as file:
    data = pkl.load(file)
    #X_train, y_train, X_val, y_val =(data[0])[s:s+t, 0:1296], (data[1])[s:s+t], (data[0])[s+t:s+t+t, 0:1296], (data[1])[s+t:s+t+t]
    #trainSet, trainLabel = data[0], data[1]
    X_val, y_val = (data[0])[s:s+t, 0:1296], (data[1])[s:s+t]
k_values = range(1, 100)

#image = np.reshape((X_val[2137]), (36, 36))


#print(type(trainSet[0][0]))

#plt.imshow(image)
#plt.show()

#image = np.reshape(knn.cut(X_val[2137]), (28, 28))

#plt.imshow(image)
#plt.show()

#newXTrain = np.zeros(shape=(t, 784))
#newXVal = np.zeros(shape=(t, 784))
#for i in range(t):
#    newXTrain[i] = knn.cut(X_train[i])
#    newXVal[i] = knn.cut(X_val[i])

#knn.model_selection_knn(X_val, X_train, y_val, y_train, k_values)

#print(knn.predict(X_val, X_train, y_train))

o = predict.predict(X_val)
print(knn.classification_error(o, y_val))

#fileObject = open('trainSet.pkl','wb')
#pkl.dump((newXTrain, y_train),fileObject)
#fileObject.close()



