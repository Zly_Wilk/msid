import numpy as np

def hamming_distance(X, X_train):
    """
    Zwróć odległość Hamminga dla obiektów ze zbioru *X* od obiektów z *X_train*.

    :param X: zbiór porównywanych obiektów N1xD
    :param X_train: zbiór obiektów do których porównujemy N2xD
    :return: macierz odległości pomiędzy obiektami z "X" i "X_train" N1xN2
    """
    x = X #np.array(X, dtype=object) #.toarray().astype(int)
    x_train = np.transpose(X_train) #np.array(np.transpose(X_train), dtype=object) #.toarray()).astype(int)
    return x.shape[1] - x @ x_train - (1 - x) @ (1 - x_train)


def sort_train_labels_knn(Dist, y):
    """
    Posortuj etykiety klas danych treningowych *y* względem prawdopodobieństw
    zawartych w macierzy *Dist*.

    :param Dist: macierz odległości pomiędzy obiektami z "X" i "X_train" N1xN2
    :param y: wektor etykiet o długości N2
    :return: macierz etykiet klas posortowana względem wartości podobieństw
        odpowiadającego wiersza macierzy Dist N1xN2

    Do sortowania użyj algorytmu mergesort.
    """
    order = Dist.argsort(kind='mergesort')
    return y[order]


def p_y_x_knn(y, k):
    """
    Wyznacz rozkład prawdopodobieństwa p(y|x) każdej z klas dla obiektów
    ze zbioru testowego wykorzystując klasyfikator KNN wyuczony na danych
    treningowych.

    :param y: macierz posortowanych etykiet dla danych treningowych N1xN2
    :param k: liczba najbliższych sasiadow dla KNN
    :return: macierz prawdopodobieństw p(y|x) dla obiektów z "X" N1xM
    """
    result = []
    for i in range(np.shape(y)[0]):
        tab = []
        for j in range(k):
            tab.append(y[i][j])
        line = np.bincount(tab, None, 10)
        # result.append([line[0] / k, line[1] / k, line[2] / k, line[3] / k, line[4] / k, line[5] / k, line[6] / k, line[7] / k, line[8] / k, line[9] / k])
        result.append(line / k)
    return result


def classification_error(p_y_x, y_true):
    """
    Wyznacz błąd klasyfikacji.

    :param p_y_x: macierz przewidywanych prawdopodobieństw - każdy wiersz
        macierzy reprezentuje rozkład p(y|x) NxM
    :param y_true: zbiór rzeczywistych etykiet klas 1xN
    :return: błąd klasyfikacji
    """
    n = len(p_y_x)
    m = len(p_y_x[0])
    res = 0
    for i in range(0, n):
        if (m - np.argmax(p_y_x[i][::-1]) - 1) != y_true[i]:
            res += 1
    return res / n


def model_selection_knn(X_val, X_train, y_val, y_train, k_values):
    """
    Wylicz bład dla różnych wartości *k*. Dokonaj selekcji modelu KNN
    wyznaczając najlepszą wartość *k*, tj. taką, dla której wartość błędu jest
    najniższa.

    :param X_val: zbiór danych walidacyjnych N1xD
    :param X_train: zbiór danych treningowych N2xD
    :param y_val: etykiety klas dla danych walidacyjnych 1xN1
    :param y_train: etykiety klas dla danych treningowych 1xN2
    :param k_values: wartości parametru k, które mają zostać sprawdzone
    :return: krotka (best_error, best_k, errors), gdzie "best_error" to
        najniższy osiągnięty błąd, "best_k" to "k" dla którego błąd był
        najniższy, a "errors" - lista wartości błędów dla kolejnych
        "k" z "k_values"
    """
    k = len(k_values)
    errors = []
    dist = hamming_distance(X_val, X_train)
    sort = sort_train_labels_knn(dist, y_train)
    for i in range(0, k):
        error = classification_error(p_y_x_knn(sort, k_values[i]), y_val)
        errors.append(error)
        best_error = min(errors)
    best_k = k_values[np.argmin(errors)]
    print(best_k)
    print(best_error)
    return best_error, best_k, errors


def estimate_a_priori_nb(y_train):
    """
    Wyznacz rozkład a priori p(y) każdej z klas dla obiektów ze zbioru
    treningowego.

    :param y_train: etykiety dla danych treningowych 1xN
    :return: wektor prawdopodobieństw a priori p(y) 1xM
    """
    return np.bincount(y_train) / len(y_train)


def estimate_p_x_y_nb(X_train, y_train, a, b):
    """
    Wyznacz rozkład prawdopodobieństwa p(x|y) zakładając, że *x* przyjmuje
    wartości binarne i że elementy *x* są od siebie niezależne.

    :param X_train: dane treningowe NxD
    :param y_train: etykiety klas dla danych treningowych 1xN
    :param a: parametr "a" rozkładu Beta
    :param b: parametr "b" rozkładu Beta
    :return: macierz prawdopodobieństw p(x|y) dla obiektów z "X_train" MxD.
    """

    maks = max(y_train) + 1
    matrix = np.zeros(shape=(maks, X_train.shape[1]))
    m = np.zeros(shape=(maks,))
    for i in range(X_train.shape[0]):
        matrix[y_train[i]] += X_train[i]
        m[y_train[i]] += 1
    for i in range(maks):
        matrix[i] = np.divide(matrix[i] + a - 1, m[i] + a + b - 2)
    return matrix


def p_y_x_nb(p_y, p_x_1_y, X):
    """
    Wyznacz rozkład prawdopodobieństwa p(y|x) dla każdej z klas z wykorzystaniem
    klasyfikatora Naiwnego Bayesa.

    :param p_y: wektor prawdopodobieństw a priori 1xM
    :param p_x_1_y: rozkład prawdopodobieństw p(x=1|y) MxD
    :param X: dane dla których beda wyznaczone prawdopodobieństwa, macierz NxD
    :return: macierz prawdopodobieństw p(y|x) dla obiektów z "X" NxM
    """
    x = X.toarray()
    p_x_1_y_rev = 1 - p_x_1_y
    x_rev = 1 - x
    res = []
    for i in range(x.shape[0]):
        success = p_x_1_y ** x[i,]
        fail = p_x_1_y_rev ** x_rev[i,]
        a = np.prod(success * fail, axis=1) * p_y
        asum = np.sum(a)
        res.append(a / asum)
    return np.array(res)


def model_selection_nb(X_train, X_val, y_train, y_val, a_values, b_values):
    """
    Wylicz bład dla różnych wartości *a* i *b*. Dokonaj selekcji modelu Naiwnego
    Byesa, wyznaczając najlepszą parę wartości *a* i *b*, tj. taką, dla której
    wartość błędu jest najniższa.

    :param X_train: zbiór danych treningowych N2xD
    :param X_val: zbiór danych walidacyjnych N1xD
    :param y_train: etykiety klas dla danych treningowych 1xN2
    :param y_val: etykiety klas dla danych walidacyjnych 1xN1
    :param a_values: lista parametrów "a" do sprawdzenia
    :param b_values: lista parametrów "b" do sprawdzenia
    :return: krotka (best_error, best_a, best_b, errors), gdzie "best_error" to
        najniższy osiągnięty błąd, "best_a" i "best_b" to para parametrów
        "a" i "b" dla której błąd był najniższy, a "errors" - lista wartości
        błędów dla wszystkich kombinacji wartości "a" i "b" (w kolejności
        iterowania najpierw po "a_values" [pętla zewnętrzna], a następnie
        "b_values" [pętla wewnętrzna]).
    """

    errors = [
        classification_error(p_y_x_nb(estimate_a_priori_nb(y_train), estimate_p_x_y_nb(X_train, y_train, a, b), X_val),
                             y_val) for a in a_values for b in b_values]
    sorted_errors_indices = np.argsort(errors)
    best_error = errors[sorted_errors_indices[0]]
    best_a = a_values[(sorted_errors_indices[0] / len(b_values)).astype(int)]
    best_b = b_values[(sorted_errors_indices[0] % len(b_values)).astype(int)]
    errors = np.array(errors).reshape(np.sqrt(len(errors)).astype(int), np.sqrt(len(errors)).astype(int))
    return best_error, best_a, best_b, errors

def cut(image):
    a = 0
    b = 0
    aFound = False
    bFound = False
    newImage = np.reshape(image, (36, 36))
    for i in range(8):
        if 0 in newImage[i, :]:
            aFound = True
        if 0 in newImage[:, i]:
            bFound = True
        if not aFound:
            a = i+1
        if not bFound:
            b = i+1

    return np.float32(np.reshape(newImage[a:a+28, b:b+28], (784, )))


def predict(X_val, X_train, y_train):
    k = 7
    dist = hamming_distance(X_val, X_train)
    sort = sort_train_labels_knn(dist, y_train)
    y = p_y_x_knn(sort, k)

    return y



def save(name):
    return
