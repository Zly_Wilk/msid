import pickle as pkl
import numpy as np


def readFile(fileName):
    with open(fileName, 'rb') as file:
        return pkl.load(file)


def getTrainingSet():
    data = readFile('train.pkl')[0]
    return data[0:50000, 0:1296]


def getTrainingLabel():
    data = readFile('train.pkl')[1]
    return data[0:50000]

def getTraining():
    data = readFile('train.pkl')
    return (data[0])[0:30000, 0:1296], (data[1])[0:30000]

def getValidationSet():
    data = readFile('train.pkl')[0]
    return data[50000:60000, 0:1296]


def getValidationLabel():
    data = readFile('train.pkl')[1]
    return data[50000:60000]

def getValidation():
    data = readFile('train.pkl')
    return (data[0])[30000:60000, 0:1296], (data[1])[30000:60000]

def softmax(W):
    expW = np.exp(W)
    return expW / expW.sum()


def sigmoid(x):
    return 1 / (1 + np.exp(-x))


def gradientSigmoid(x):
    return sigmoid(x) * (1 - sigmoid(x))


def learn():

    feature_set, laabels = getTraining()
    # = getTrainingLabel()

    one_hot_labels = np.zeros((50000, 10))
    for i in range(50000):
        one_hot_labels[i, laabels[i]] = 1

    wh = np.random.rand(1296, 100)
    bh = np.random.randn(100)

    wo = np.random.rand(100, 10)
    bo = np.random.randn(10)
    lr = 10e-10

    error_cost = []



    for epoch in range(50000):
        ############# feedforward

        # Phase 1
        zh = np.dot(feature_set, wh) + bh
        ah = sigmoid(zh)

        # Phase 2
        zo = np.dot(ah, wo) + bo
        ao = softmax(zo)

        ########## Back Propagation

        ########## Phase 1

        dcost_dzo = ao - one_hot_labels
        dzo_dwo = ah

        dcost_wo = np.dot(dzo_dwo.T, dcost_dzo)

        dcost_bo = dcost_dzo

        ########## Phases 2

        dzo_dah = wo
        dcost_dah = np.dot(dcost_dzo, dzo_dah.T)
        dah_dzh = gradientSigmoid(zh)
        dzh_dwh = feature_set
        dcost_wh = np.dot(dzh_dwh.T, dah_dzh * dcost_dah)

        dcost_bh = dcost_dah * dah_dzh

        # Update Weights ================

        wh -= lr * dcost_wh
        bh -= lr * dcost_bh.sum(axis=0)

        wo -= lr * dcost_wo
        bo -= lr * dcost_bo.sum(axis=0)

        if epoch % 200 == 0:
            loss = np.sum(-one_hot_labels * np.log(ao))
            print('Loss function value: ', loss)
            error_cost.append(loss)


def classification_error(p_y_x, y_true):
    """
    Wyznacz błąd klasyfikacji.

    :param p_y_x: macierz przewidywanych prawdopodobieństw - każdy wiersz
        macierzy reprezentuje rozkład p(y|x) NxM
    :param y_true: zbiór rzeczywistych etykiet klas 1xN
    :return: błąd klasyfikacji
    """
    n = len(p_y_x)
    m = len(p_y_x[0])
    res = 0
    for i in range(0, n):
        if (m - np.argmax(p_y_x[i][::-1]) - 1) != y_true[i]:
            res += 1
    return res / n

def test():
    feature_set, laabels = getValidation()
    predicted =[]

    weights = np.transpose([[1.23881363e-306],[ 1.45175494e-307],[ 1.84740267e-308],[ 2.57679146e-206],[1.96736313e-307],
               [ 2.06780052e-307], [7.53263017e-308], [2.00000000e-001],
  [2.86954494e-307], [1.20298294e-307]])
    """
 [1.23881363e-306, 1.45175494e-307, 1.84740267e-308, 2.57679146e-206,
  1.96736313e-307, 2.06780052e-307, 7.53263017e-308, 2.00000000e-001,
  2.86954494e-307, 1.20298294e-307],
 [1.23881363e-306, 1.45175494e-307 ,1.84740267e-308, 2.57679146e-206,
  1.96736313e-307, 2.06780052e-307 ,7.53263017e-308, 2.00000000e-001,
  2.86954494e-307, 1.20298294e-307],
 [1.23881363e-306, 1.45175494e-307, 1.84740267e-308 ,2.57679146e-206,
  1.96736313e-307, 2.06780052e-307, 7.53263017e-308, 2.00000000e-001,
  2.86954494e-307, 1.20298294e-307],
 [1.23881363e-306, 1.45175494e-307, 1.84740267e-308, 2.57679146e-206,
  1.96736313e-307, 2.06780052e-307, 7.53263017e-308 ,2.00000000e-001,
  2.86954494e-307, 1.20298294e-307]]
    """
    for i in range (5):
        predicted.append(softmax(np.dot(np.reshape(feature_set[i], (1296,1)), weights)))

    print(predicted)
