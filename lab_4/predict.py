# --------------------------------------------------------------------------
# ------------  Metody Systemowe i Decyzyjne w Informatyce  ----------------
# --------------------------------------------------------------------------
#  Zadanie 4: Zadanie zaliczeniowe
#  autorzy: A. Gonczarek, J. Kaczmar, S. Zareba, M. Zieba, P. Dąbrowski
#  2019
# --------------------------------------------------------------------------

import pickle as pkl
import numpy as np


def predict(x):
    """
    Dokonaj predykcji dla obiektów w macierzy x. Zwróć wektor predykowanych
    etykiet z zakresu {0, ..., 9}, oznaczający numer klasy, do której model
    przypisał daną obserwację.
    :param x: macierz obiektów do predykcji o wymiarach NxD
    :return: wektor predykowanych etykiet o wymiarach Nx1
    """

    a = x.shape[0]
    toReturn = np.zeros(shape=(a, 1)).astype(int)
    newX = np.zeros(shape=(a, 784))

    for i in range(a):
        newX[i] = cut(x[i])
    with open('trainSet.pkl', 'rb') as file:
        data = pkl.load(file)
        trainSet, trainLabel = data[0], data[1]
    dist = hammingDistance(newX, trainSet)
    sort = sortedTrainLabels(dist, trainLabel)
    y = knn(sort)
    for i in range(a):
        toReturn[i] = np.argmax(y[i])
    return toReturn


def hammingDistance(matrix0, matrix1):

    matrix1T = np.transpose(matrix1)
    return matrix0.shape[1] - matrix0 @ matrix1T - (1 - matrix0) @ (1 - matrix1T)


def sortedTrainLabels(hD, array):

    order = hD.argsort(kind='mergesort')
    return array[order]


def knn(matrix):

    k = 7
    arrayToReturn = []
    for i in range(np.shape(matrix)[0]):
        tab = []
        for j in range(k):
            tab.append(matrix[i][j])
        line = np.bincount(tab, None, 10)
        arrayToReturn.append(line / k)
    return arrayToReturn


def cut(image):
    a = 0
    b = 0
    aFound = False
    bFound = False
    newImage = np.reshape(image, (36, 36))
    for i in range(8):
        if 0 in newImage[i, :]:
            aFound = True
        if 0 in newImage[:, i]:
            bFound = True
        if not aFound:
            a = i+1
        if not bFound:
            b = i+1

    return np.float32(np.reshape(newImage[a:a+28, b:b+28], (784, )))
